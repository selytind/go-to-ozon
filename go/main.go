package main

type E struct {
	i int
	v uint
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

// Функция поиска неотсортированного подмассива для
// XVII Задача о поиске не отсортированного подмассива.
// По условию и комментариям к нему, входной массив
// отсортирован по возрастанию.
// Если массив отсортирован полностью, возвращает (0, 0).
func findUnsortedSubarray(array []uint) (left uint, right uint) {
	leftE, rightE := E{-1, 0}, E{-1, 0}

	N := len(array)

	lRes, rRes := N, -1

	for i := 0; i < N - 1; i++ {
		j := N - i - 1

		// тут ищем неотсортированный эл-т
		if leftE.i < 0 && array[i] > array[i+1] {
			lRes = min(i, lRes)
			leftE = E{i, array[i]}
		}

		if rightE.i < 0 && array[j] < array[j-1] {
			rRes = max(j, rRes)
			rightE = E{j, array[j]}
		}

		// тут ищем, куда его вставить
		if leftE.i > -1 && array[i+1] >= leftE.v {
			rRes = max(i, rRes)
			leftE.i = -1
		}

		if rightE.i > -1 && array[j-1] <= rightE.v {
			lRes = min(j, lRes)
			rightE.i = -1
		}
	}

	// если так и не нашли, куда вставить, берем конец массива
	if leftE.i > -1 {
		rRes = N - 1
	}

	if rightE.i > -1 {
		lRes = 0
	}

	if rRes > 0 {
		left = uint(lRes)
		right = uint(rRes)
	}

	return
}

func main()  {
	findUnsortedSubarray([]uint{1, 2, 1, 3, 4, 10, 5})
}
